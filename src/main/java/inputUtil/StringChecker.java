package inputUtil;

public class StringChecker {
    public static boolean checkForEmptyOrNull(String s){
        if(s != null && !(s.trim().equals(""))){
            return true;
        }
        return false;
    }

    public static boolean checkContinueIsTrue(String s){
        if(s.matches("(?i:y)|(?i:yes)")){
            return true;

        }
        else {
            return false;
        }
    }
}
