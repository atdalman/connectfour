package gameUtil;


import exceptions.ColumnFullException;
import model.GameBoard;
import model.Player;

public class GameLogic {

    public static GameBoard insertPiece(Player currentPlayer, GameBoard gb, int column, int height) throws ColumnFullException {
        if (height == 0) {
            throw new ColumnFullException();
        } else if (gb.getBoard()[column - 1][height - 1] == null) {
            gb.getBoard()[column - 1][height - 1] = currentPlayer.getColor();
            checkGameState(gb, column - 1, height - 1);

            return gb;
        } else {
            return insertPiece(currentPlayer, gb, column, height - 1);
        }

    }

    // Just switches the current player in the GameBoard object
    public static GameBoard switchPlayer(GameBoard gb) {
        if (gb.getCurrentPlayer().equals(gb.getGreenPlayer())) {
            gb.setCurrentPlayer(gb.getRedPlayer());
        } else {
            gb.setCurrentPlayer(gb.getGreenPlayer());
        }
        return gb;
    }

    public static GameBoard checkGameState(GameBoard gb, int inputColumn, int inputRow) {
        // Check if all columns are full.  If not check for a win condition
        for (int i = 0; i < GameBoard.BOARD_WIDTH; i++) {
            if (gb.getBoard()[i][0] == null) {
                return checkForWinCondition(gb, inputColumn, inputRow);
            }
        }

        // If all columns are full, check for a win condition.  If none are found, game is a draw
        if(checkForWinCondition(gb, inputColumn, inputRow).getGameState().getWinner() == null) {
            // If all of the last columns are full, just set game over to true and leave the "winner" as null
            gb.getGameState().setGameOver(true);
        }


        return gb;
    }

    public static GameBoard checkForWinCondition(GameBoard gb, int column, int row) {
        if (checkHorizontal(gb.getCurrentPlayer().getColor(), row, gb.getBoard())) {
            gb.getGameState().setGameOver(true);
            gb.getGameState().setWinner(gb.getCurrentPlayer());
        }
        if (checkVertical(gb.getCurrentPlayer().getColor(), column, gb.getBoard())) {
            gb.getGameState().setGameOver(true);
            gb.getGameState().setWinner(gb.getCurrentPlayer());
        }
        if (checkDiagonalUp(gb.getCurrentPlayer().getColor(), column, row, gb.getBoard())) {
            gb.getGameState().setGameOver(true);
            gb.getGameState().setWinner(gb.getCurrentPlayer());
        }
        if (checkDiagonalDown(gb.getCurrentPlayer().getColor(), column, row, gb.getBoard())) {
            gb.getGameState().setGameOver(true);
            gb.getGameState().setWinner(gb.getCurrentPlayer());
        }
        return gb;
    }

    public static boolean checkHorizontal(String color, int row, String[][] board) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < GameBoard.BOARD_WIDTH; i++) {
            builder.append(board[i][row]);
        }
        if (builder.toString().contains(color + color + color + color)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(String color, int column, String[][] board) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < GameBoard.BOARD_HEIGHT; i++) {
            builder.append(board[column][i]);
        }
        if (builder.toString().contains(color + color + color + color)) {
            return true;
        }
        return false;
    }

    public static boolean checkDiagonalUp(String color, int column, int row, String[][] board) {
        int diagonalSum = row + column;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < GameBoard.BOARD_HEIGHT; i++) {
            for (int j = 0; j < GameBoard.BOARD_WIDTH; j++) {
                if (j + i == diagonalSum) {
                    builder.append(board[j][i]);
                }
            }
        }
        if (builder.toString().contains(color + color + color + color)) {
            System.out.println("Diagonal up win");
            return true;
        }
        return false;
    }

    public static boolean checkDiagonalDown(String color, int column, int row, String[][] board) {
        int diagonalDifference = row - column;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < GameBoard.BOARD_HEIGHT; i++) {
            for (int j = 0; j < GameBoard.BOARD_WIDTH; j++) {
                if (i - j == diagonalDifference) {
                    builder.append(board[j][i]);
                }
            }
        }
        if (builder.toString().contains(color + color + color + color)) {
            System.out.println("Diagonal down win");
            return true;
        }
        return false;
    }

    public static void printBoard(GameBoard board) {
        for (int i = 0; i < GameBoard.BOARD_HEIGHT; i++) {
            for (int j = 0; j < GameBoard.BOARD_WIDTH; j++) {
                if (board.getBoard()[j][i] == null) {
                    System.out.print("|" + GameBoard.EMPTY_SLOT);
                } else {
                    System.out.print("|" + board.getBoard()[j][i]);
                }

            }
            System.out.print("|");
            System.out.println();
        }
    }


}
