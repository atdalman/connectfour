package model;

public class GameState {

    private boolean gameOver;
    private Player winner;

    public GameState() {
        this.gameOver = false;
        this.winner = null;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameState gameState = (GameState) o;

        if (gameOver != gameState.gameOver) return false;
        return winner != null ? winner.equals(gameState.winner) : gameState.winner == null;
    }

    @Override
    public int hashCode() {
        int result = (gameOver ? 1 : 0);
        result = 31 * result + (winner != null ? winner.hashCode() : 0);
        return result;
    }
}
