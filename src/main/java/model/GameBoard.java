package model;

import java.util.Arrays;

public class GameBoard {
    public static final int BOARD_WIDTH = 7;
    public static final int BOARD_HEIGHT = 6;
    public static final String EMPTY_SLOT = " ";
    public static final String RED_DISC = "R";
    public static final String GREEN_DISC = "G";

    private String[][] board;
    private Player redPlayer;
    private Player greenPlayer;
    private Player currentPlayer;
    private GameState gameState;

    public GameBoard(Player redPlayer, Player greenPlayer) {
        this.board = new String[BOARD_WIDTH][BOARD_HEIGHT];
        this.redPlayer = redPlayer;
        this.greenPlayer = greenPlayer;
        this.currentPlayer = redPlayer;
        this.greenPlayer.setColor(GREEN_DISC);
        this.redPlayer.setColor(RED_DISC);
        this.gameState = new GameState();
        this.gameState.setGameOver(false);
        this.gameState.setWinner(null);
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public String[][] getBoard() {
        return board;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public void setBoard(String[][] board) {
        this.board = board;
    }

    public Player getRedPlayer() {
        return redPlayer;
    }

    public void setRedPlayer(Player redPlayer) {
        this.redPlayer = redPlayer;
    }

    public Player getGreenPlayer() {
        return greenPlayer;
    }

    public void setGreenPlayer(Player greenPlayer) {
        this.greenPlayer = greenPlayer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameBoard gameBoard = (GameBoard) o;

        if (!Arrays.deepEquals(board, gameBoard.board)) return false;
        if (redPlayer != null ? !redPlayer.equals(gameBoard.redPlayer) : gameBoard.redPlayer != null) return false;
        if (greenPlayer != null ? !greenPlayer.equals(gameBoard.greenPlayer) : gameBoard.greenPlayer != null)
            return false;
        if (currentPlayer != null ? !currentPlayer.equals(gameBoard.currentPlayer) : gameBoard.currentPlayer != null)
            return false;
        return gameState != null ? gameState.equals(gameBoard.gameState) : gameBoard.gameState == null;
    }

    @Override
    public int hashCode() {
        int result = Arrays.deepHashCode(board);
        result = 31 * result + (redPlayer != null ? redPlayer.hashCode() : 0);
        result = 31 * result + (greenPlayer != null ? greenPlayer.hashCode() : 0);
        result = 31 * result + (currentPlayer != null ? currentPlayer.hashCode() : 0);
        result = 31 * result + (gameState != null ? gameState.hashCode() : 0);
        return result;
    }
}