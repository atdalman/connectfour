import exceptions.ColumnFullException;
import gameUtil.GameLogic;
import inputUtil.StringChecker;
import model.GameBoard;
import model.Player;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String args[]) {
        boolean continueGame = true;
        boolean nameSuccessful = false;
        String input;
        Player redPlayer = new Player("Player 1");
        Player greenPlayer = new Player("Player 2");
        System.out.println("Hello, welcome to Connect Four");
        while(!nameSuccessful) {
            System.out.print("Please enter the name of Red Player: ");
            input = scanner.nextLine();
            if (StringChecker.checkForEmptyOrNull(input)) {
                 redPlayer = new Player(input);
                nameSuccessful = true;

            } else{
                System.out.print("Not a valid name. ");
            }
        }
        nameSuccessful = false;
        while(!nameSuccessful) {
            System.out.print("Please enter the name of Green Player: ");
            input = scanner.nextLine();
            if (StringChecker.checkForEmptyOrNull(input)) {
                 greenPlayer = new Player(input);
                nameSuccessful = true;
            } else{
                System.out.print("Not a valid name. ");

            }
        }

        // If players elect to play again, game board resets (with the same player names)
        while (continueGame) {
            GameBoard board = new GameBoard(redPlayer, greenPlayer);
            continueGame = runGame(board);
        }

        System.out.print("Thanks for playing!");

    }

    private static boolean runGame(GameBoard board) {
        int columnInput;
        String continueInput;

        boolean insertSuccessful;

        // Game runs until gameState.gameOver is set to true either by a win condition or a full board
        while (!board.getGameState().isGameOver()) {
            insertSuccessful = false;

            // Loops until an insert is successful
            while (!insertSuccessful) {
                GameLogic.printBoard(board);
                System.out.print(board.getCurrentPlayer().getName() + "[" + board.getCurrentPlayer().getColor() + "] - choose column (1-7): ");

                try {
                    columnInput = Integer.parseInt(scanner.nextLine());
                    GameLogic.insertPiece(board.getCurrentPlayer(), board, columnInput, GameBoard.BOARD_HEIGHT);

                } catch (NumberFormatException e) {
                    System.out.println("Not a valid number.\n");
                    continue;
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Column does not exist.\n");
                    continue;
                } catch (ColumnFullException e) {
                    System.out.println("Selected column is full.\n");
                    continue;
                }
                insertSuccessful = true;
            }
            GameLogic.switchPlayer(board);
        }
        GameLogic.printBoard(board);

        // If the gameOver field is set to true but there isn't a winner set, the game is assumed to be a draw
        if (board.getGameState().getWinner() == null) {
            System.out.println("Game ends in a draw!");
        } else {
            System.out.println(board.getGameState().getWinner().getName() + " wins!");
        }

        System.out.print("Start a new game? (y/n): ");
        continueInput = scanner.nextLine();
        if (StringChecker.checkContinueIsTrue(continueInput)) {
            return true;
        } else {
            return false;
        }
    }
}
