Connect Four Instructions
Build: Project builds a jar with 'mvn package'.

To play, first enter valid names for each player.  Take turns entering discs in empty columns in an attempt to connect four of your discs.  

Win condition: Either player has three or more discs in a row either vertically, diagonally, or horizontally. Game ends in a draw if there are no available moves left.
